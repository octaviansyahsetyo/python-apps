FROM python:latest
LABEL maintainer='octaviansyahsb'

WORKDIR '/usr/app/src'
COPY app.py ./

EXPOSE 8080
CMD ['python', '-m', 'flask', 'run']